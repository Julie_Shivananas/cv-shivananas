# CV Shivananas 🐙

*UI UX designer et Front end développeuse 🌱*
Oui je suis Junior et j'ai une longue vie pour apprendre 🐛

Sauve la planète et propose-moi un entretien 🌻

Mes valeurs ? 
Féminisme
Diversité
Humour
Écolo
Amours des animaux et des plantes !

Humanité ✌

## En quelques mots pour en savoir plus...

J'ai été Graphiste.
J'ai tenté de lancer mon biz' d'illustrations (puis j'ai dû arrêter).
En 2021 j'attaque une reconversion dans le développement Front-end afin de débloquer d'autres skills !
Je découvre par la même occasion l'UI et l'UX design : ça MATCH et J'AIME ÇA !

En stage longue durée, je bouillonne sur du React : oui c'est pas facile, et heureusement car j'apprends la logique de cette bibli'.
Mon stage se terminant, je continue de m'auto-former, je carbure car j'EN VEUX ! Je te cherche, toi, mon entreprise safe et patiente qui aime le partage de connaissances. On échange et on avance ensemble !

J'ai créée un Discord qui fait matcher des __Dev et des Designer__, c'est le bon plan pour travailler en mode projet et se booster ENSEMBLE durant ces moments de flou (reconversion, sans emploi mais auto-formation, side-projects...).

J'aime parler à travers un podcast : j'y évoque ma reconversion pro' dans le digital, ainsi que mon parcours en tant qu'apprenante et tu verras que ça n'a pas été facile.
Go sur [TeckFrizz](https://open.spotify.com/show/5gpS5abUfruHDL6IkaVTGk) sur ton appli d'écoute !


*Alors, qu'est-ce que t'en dis ?*

RDV sur [Behance](https://www.behance.net/julietzns/) pour voir mes projets Design, et mon [Figma](https://www.figma.com/@juliethezenas) pour quelques prototypes. 


🌻
